function increment() {

  numbers.nb += factor;
  numbers.pixNb += factor;

  afterIncrement();
}

function afterIncrement() {
  showConsole(factor, '+', '#00FF00');
  fillPixelCounter();
  showButtons();
  counterUnit.innerHTML = '<li>' + numbers.nb + '</li>';
  checkCredits();
}


function resetpixelCounter(pix) {
  for (let i = 0; i < pix.length; i++) {
    pix[i].removeAttribute('style');
  }
}

function fillPixelCounter() {
  for (let i = 0; i < pix.length; i++) {
    const el = pix[i];
    el.style.outline = '';
    if (i <= numbers.pixNb) {
      el.style.outline = '1px solid';
    }
  }
}

function showConsole(num, operator, color, position) {
  var chrisme = (operator == '+') ? 'chrismeV' : 'chrismeR';
  var style = 'color:' + color + '; text-align:' + position + ';';
  var el = '<div style="' + style + '">' + operator + num + '<img src="assets/img/' + chrisme + '.bmp"/></div>';
  consoleTheo.innerHTML = el;
  setTimeout(() => {
    consoleTheo.innerHTML = '';
  }, 250);
}

function upgrade() {
  if (numbers.nb >= 8 && numbers.nb >= numbers.upPrice) {
    factor = factor + 1;
    numbers.nb -= numbers.upPrice;
    refreshCounter(counterUnit, numbers.nb);
    showConsole(numbers.upPrice, '-', '#FF0000');
    fillPixelCounter();
    numbers.upPrice += factor;
    checkCredits();
  }
}

function buySlot() {
  if (numbers.nb >= numbers.slotPrice) {
    numbers.nb -= numbers.slotPrice;
    refreshCounter(counterUnit, numbers.nb);
    showConsole(numbers.slotPrice, '-', '#FF0000');
    fillPixelCounter();
    showSlot();

    buttonBuyGlif.style.display = 'block';
    numbers.slotPrice = Math.round(numbers.slotPrice * 1.25);
    checkCredits();
  }
}

function buyGlif() {
  if (numbers.nb >= glifPrice && numbers.nbSlot > numbers.nbGlif) {
    numbers.nb -= glifPrice;
    refreshCounter(counterUnit, numbers.nb);
    showConsole(glifPrice, '-', '#FF0000');
    fillPixelCounter();
    showGlif();
    showButtons();
    checkCredits();
  }
}

function refreshCounter(counter, ind) {
  counter.innerHTML = '<li>' + ind + '</li>';
}


function killGlif(image) {
  image.setAttribute('src', 'assets/img/red.bmp');
  var intervalId = null;
  var timer = 50;
  var kill = function() {
    if (timer > 0) {
      var op = Math.floor((Math.random() * 2) + 0);
      image.style.opacity = op;
      timer--;
    } else if (timer == 0) {
      image.style.opacity = 1;
      image.setAttribute('src', 'assets/img/black.bmp');
      image.setAttribute('data-nb', '0')
      updateImgsArray()
      showButtons();
      clearInterval(intervalId);
    }
  };
  intervalId = setInterval(kill, 8);
  numbers.nbGlif--;

}

function setFusion(image) {

  var theGlif = parseInt(image.getAttribute('data-nb'));
  if (theGlif > 0) {
    if (image.classList.contains('select')) {
      image.classList.remove('select');
    } else {
      image.classList.add('select');
    }
  }
}

function makeFusion() {
  var select = container.getElementsByClassName('select');
  if (select.length > 1) {
    var nbFusion = 0;
    for (let i = 0; i < select.length; i++) {
      var slc = select[i];
      var theGlif = parseInt(slc.getAttribute('data-nb'));
      nbFusion += theGlif;
      if (i != 0) {
        killGlif(slc);
        subGlif(1);

      }
    }
    select[0].setAttribute('src', 'assets/img/gifs/' + nbFusion + '.gif');
    select[0].setAttribute('data-nb', nbFusion);
    for (let i = 0; i < boxes.length; i++) {
      const img = boxes[i];
      img.classList.remove('select');
    }
    checkCredits()
  }
}

function checkCredits() {
  (numbers.upPrice > numbers.nb) ? buttonUp.classList.add('gray'): buttonUp.classList.remove('gray');
  (numbers.slotPrice > numbers.nb) ? buttonBuySlot.classList.add('gray'): buttonBuySlot.classList.remove('gray');
  if (numbers.nb < glifPrice || numbers.nbSlot <= numbers.nbGlif) {
    buttonBuyGlif.classList.add('gray');
  } else {
    buttonBuyGlif.classList.remove('gray');
  }
}

function randNb(min, max) {
  return Math.floor((Math.random() * max) + min)
}

function setPosition() {
  randX = propagation[randNb(0, propagation.length)];
  randY = propagation[randNb(0, propagation.length)];
  var randPrevPos = randNb(0, prevPos.length);
  newPosX = prevPos[randPrevPos].x + randX;
  newPosY = prevPos[randPrevPos].y + randY;
}

function storeCoordinate(xVal, yVal, array) {
  array.push({ x: xVal, y: yVal });
}


function showSlot() {
  var glifImg = new Image();

  glifImg.style.left = posX + 'px';
  glifImg.style.top = posY + 'px';

  setPosition();
  var nope = true;
  while (nope == true) {
    const check = ([x, y]) => prevPos.some(o => o.x === x && o.y === y);
    if (check([newPosX, newPosY]) == true) {
      setPosition();
    } else {
      nope = false;
    }
  }

  posX = newPosX;
  posY = newPosY;

  storeCoordinate(posX, posY, prevPos);
  glifImg.setAttribute('data-nb', '0')
  glifImg.src = 'assets/img/black.bmp'
  glifImg.style.left = posX + 'px';
  glifImg.style.top = posY + 'px';
  container.appendChild(glifImg);
  numbers.nbSlot++;
  glifImg.addEventListener('click', (bx) => {
    setFusion(glifImg);
  })
  for (let i = 0; i < buttons.length; i++) {
    buttonBuyFusion.addEventListener('click', makeFusion);
    const button = buttons[i];
    button.addEventListener('mouseover', (btn) => {
      showPrice(button);
    })
    button.addEventListener('mouseout', hidePrice);

  }
}

function showGlif() {
  var imgs = container.getElementsByTagName('img');
  for (let i = 0; i < imgs.length; i++) {
    let image = imgs[i];
    let theGlif = parseInt(image.getAttribute('data-nb'));
    if (theGlif == 0) {
      image.setAttribute('src', 'assets/img/gifs/1.gif');
      image.setAttribute('data-nb', '1');
      numbers.nbGlif++;
      numbers.maxGem++;
      updateImgsArray();
      console.log('gems: ' + numbers.maxGem)
      console.log('glifs: ' + numbers.nbGlif)
      addGlif(numbers.nbGlif, numbers.maxGem);
      return;
    }
  }

}

function showSlotOnLoad() {
  // for (let i = 0; i < numbers.nbSlot; i++) {
  //   let image = boxes[i];
  //   image.style.outline = '1px solid rgb(82,82,82)';
  //   buttonBuyGlif.style.display = 'block';
  // }
}

function hideSlotAndGlif() {
  for (let i = 0; i < boxes.length; i++) {
    let image = boxes[i];
    image.style.outline = '0px solid rgb(82,82,82)';
    image.setAttribute('src', 'assets/img/black.bmp');
    image.setAttribute('data-nb', '0')
  }
}

function updateImgsArray() {
  for (let i = 0; i < boxes.length; i++) {
    let image = boxes[i];
    let dataNb = image.getAttribute('data-nb');
    numbers.imgs[i] = dataNb;
  }
}

function getGlifsOnLoad() {
  // console.log(numbers.imgs)
  // if (numbers.imgs[1]) {
  //   for (let i = 0; i < boxes.length; i++) {
  //     let image = boxes[i];
  //     let nb = numbers.imgs[i];
  //     if (nb && nb != '0') {
  //       image.setAttribute('src', 'assets/img/gifs/' + nb + '.gif');
  //       image.setAttribute('data-nb', nb);
  //     }
  //   }
  // }
}

function showButtons() {
  if (numbers.nb >= 8) {
    buttonUp.style.display = 'block';
  }
  if (numbers.nb >= 256) {
    buttonBuySlot.style.display = 'block';
  }
  if (numbers.pixNb >= 256) {
    numbers.pixNb = 0;
  }
  if (numbers.nbGlif >= 2) {
    buttonBuyFusion.style.display = 'block';
  }
  if (numbers.nb <= 1) {
    buttonUp.style.display = 'none';
    buttonBuySlot.style.display = 'none';
    buttonBuyFusion.style.display = 'none';
    buttonBuyGlif.style.display = 'none';
    numbers.pixNb = 0;

  }
}

function showSentences(index) {
  var actualSentences = sentences[index];
  if (index > 0) {
    var prevSentences = sentences[index - 1];
    prevSentences.style.display = 'none';
  }
  actualSentences.style.display = 'block';
}

function randomSentences() {
  for (let i = 0; i < sentences.length; i++) {
    if (sentences[i].style.display == 'block') {
      var sentence = sentences[i].getElementsByTagName('p');
      var rand = Math.floor((Math.random() * sentence.length) + 0);
      for (let j = 0; j < sentence.length; j++) {
        sentence[j].style.display = 'none';
      }
      sentence[rand].style.display = 'inline';
    }
  }
}

function miningPixels() {
  numbers.nb = numbers.nb + numbers.maxGem;
  if (numbers.nbGlif != 0) {
    refreshCounter(counterUnit, numbers.nb);
    showConsole(numbers.maxGem, '+', '#00FF00', 'right');
    fillPixelCounter();
  }
}

function showPrice(btn) {
  let id = btn.classList[1];
  let color = btn.classList[2];
  let message = '';
  let chrisme = (color == 'gray') ? 'chrismeG' : 'chrisme'
  let image = '<img src="assets/img/' + chrisme + '.bmp"/></div>'
  if (id == 'buttonUp') {
    message = '<span style="color:' + color + ';">+' + factor + image + '</span>';
  } else if (id == 'buttonUpgrade') {
    message = '<span style="color:' + color + ';">-' + numbers.upPrice + image + '</span>';
  } else if (id == 'buttonSlot') {
    message = '<span style="color:' + color + ';">-' + numbers.slotPrice + image + '</span>';
  } else if (id == 'buttonGlif') {
    message = '<span style="color:' + color + ';">-' + glifPrice + image + '</span>';
  }
  infoBox.innerHTML = message;
}

function hidePrice() {
  infoBox.innerHTML = '';
}


function cheat() {
  document.addEventListener('keydown', (event) => {
    const key = event.key;

    if (key == 'Shift') {
      numbers.nb += 1000;
      fillPixelCounter();
      showButtons();
      counterUnit.innerHTML = '<li>' + numbers.nb + '</li>';
      checkCredits();
    } else if (key == 'Escape') {
      factor = 1;
      numbers = {
        nb: 0,
        pixNb: 0,
        nbFusion: 0,
        imgNb: 1,
        maxGem: 0,
        factor: 1,
        nbSlot: 0,
        nbGlif: 0,
        upPrice: factor * 8,
        slotPrice: 256,
        imgs: [{
          0: 'no'
        }]
      }
      afterIncrement();
      showSlotOnLoad();
      hideSlotAndGlif();
    }
  })
}