var savegame = JSON.parse(localStorage.getItem("save"));

var glifPrice = 256;
var factor = 1;

var interfaces = document.getElementsByTagName('main');
for (let i = 0; i < interfaces.length; i++) {
  const interface = interfaces[i];

  var numbers = {
    nb: 0,
    pixNb: 0,
    nbFusion: 0,
    imgNb: 1,
    maxGem: 0,
    factor: 1,
    nbSlot: 0,
    nbGlif: 0,
    upPrice: factor * 8,
    slotPrice: 256,
    imgs: [{
      0: 'no'
    }]
  }

  var buttons = interface.getElementsByClassName('button');
  var buttonTheo = interface.getElementsByClassName('buttonUp')[0];
  var buttonUp = interface.getElementsByClassName('buttonUpgrade')[0];
  var buttonBuySlot = interface.getElementsByClassName('buttonSlot')[0];
  var buttonBuyGlif = interface.getElementsByClassName('buttonGlif')[0];
  var buttonBuyFusion = interface.getElementsByClassName('buttonFusion')[0];

  var infoBox = interface.getElementsByClassName('infoBox')[0];
  var consoleTheo = interface.getElementsByClassName('console')[0];

  var counterUnit = interface.getElementsByClassName('incrementUnit')[0];
  var pixelCounter = interface.getElementsByClassName('pixelCounter')[0];
  var pix = pixelCounter.getElementsByTagName('td');
  var log = interface.getElementsByClassName('console')[0];

  var imageContainer = interface.getElementsByClassName('images')[0];
  // var boxes = imageContainer.getElementsByTagName('td');
  var imgs = imageContainer.getElementsByTagName('img');

  var sentences = interface.getElementsByClassName('sentences');

  var list = interface.getElementsByClassName('list');

  var j = 0;
  var interval = 100;
  var glifSize = 16;

  var windowW = window.innerWidth;
  var windowY = window.innerHeight;
  var propagation = [glifSize, -glifSize, 0];
  var posX = windowW / 2 - 8;
  var posY = windowY / 2 - 8;
  var box = document.getElementById('propagation');
  var prevPos = [];
  var newpos = [];
  var newPosX, newPosY, randX, randY;
  var container = document.getElementById('propagation');
  var boxes = container.getElementsByTagName('img');

  storeCoordinate(posX, posY, prevPos);

  if (savegame) {
    factor = savegame.factor
    numbers = savegame;

    // init
    getGlifsOnLoad();
    afterIncrement();
    showSlotOnLoad();
  }

  buttonBuySlot.addEventListener('click', buySlot);
  buttonBuyGlif.addEventListener('click', buyGlif);
  buttonUp.addEventListener('click', upgrade);
  buttonTheo.addEventListener('click', increment);

  for (let i = 0; i < boxes.length; i++) {
    const box = boxes[i];
    box.addEventListener('click', (bx) => {
      setFusion(box);
    })

    for (let i = 0; i < buttons.length; i++) {
      buttonBuyFusion.addEventListener('click', makeFusion);
      const button = buttons[i];
      button.addEventListener('mouseover', (btn) => {
        showPrice(button);
      })
      button.addEventListener('mouseout', hidePrice);

    }
  }

  setInterval(() => {
    randomSentences();
  }, 1000)

  setInterval(() => {
    miningPixels();
    localStorage.setItem("save", JSON.stringify(numbers));
  }, 1500);

  setInterval(() => {
    for (let i = 0; i < boxes.length; i++) {
      var image = boxes[i];
      var rand = Math.floor((Math.random() * 4) + 0) * 90;
      image.style.transform = 'rotate(' + rand + 'deg)';
    }
  }, 100);

  // chut!
  cheat();

}