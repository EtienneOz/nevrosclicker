var gem = 0;
var glif = 0;
var num = 0;
var phrase = "";
var mot = "";
var gemArray = [];
var goodArray = ["&nbsp;"];
var phraseArray = [];
var total = 0;
var n = 1;
var check = false;

function subGem(num) {
  gem = gem - num;
  updateArray();
}

function addGem(num) {
  gem = num;
  updateArray();
}

function subGlif(num) {
  if (glif > 0 + num) {
    glif = glif - num;
    updateArray();
  }
}

function addGlif(glifnew, gemnew) {
  gem = gemnew;
  glif = glifnew;
  updateArray();
}

function convertGem(phraseArray) {
  total = 0;
  gemArray = [];
  for (l = 0; l < phraseArray.length; l++) {
    str = phraseArray[l];
    for (m = 0; m < str.length; m++) {
      num = str[m].charCodeAt(0) - 96;
      total = total + num;
    }
    gemArray.push(total);
  }
}

function checkGem(num) {
  return num <= gem;
}

function removeDiacritics(str) {
  return str.replace(/[^\u0000-\u007E]/g, function(a) {
    return diacriticsMap[a] || a;
  });
}

function updateArray() {

  goodArray = [];

  for (j = 0; j < array.length; j++) {
    phrase = array[j];
    phraseClean = removeDiacritics(phrase);
    phraseClean = phraseClean.replace(/\.|\,| \?|\?|\!|\#|\$|\%|\&|\(|\)|\*|\+|\/|\\|\:|\<|\>|\=|\@|\[|\]|\^|\||\_|\-|\'|\’|\;/g, " ");
    phraseArray = phraseClean.split(" ");
    convertGem(phraseArray);

    for (k = 0; k < phraseArray.length; k++) {
      if (gemArray.every(checkGem) == true && phrase.length == glif) {
        check = true;
      } else {
        check = false;
      }
    }

    if (check == true) {
      goodArray.push(phrase);
    }

  }
  return gem;
}

function displayTxt() {
  var rand = goodArray[Math.floor(Math.random() * goodArray.length)];
  if (rand) {
    document.getElementById("text").innerHTML = rand;
    addWords(rand);
  } else {
    document.getElementById("text").innerHTML = "&nbsp;";
  }
  clearTxt(1500);
}

function clearTxt(time) {
  setTimeout(function() { document.getElementById("text").innerHTML = "&nbsp;" }, time);
}

window.onload = function() {
  displayTxt();
};

setInterval(() => {
  displayTxt();
}, 2000);

function addWords(rand) {
  var words = rand.split(' ');
  for (let i = 0; i < words.length; i++) {
    const word = words[i];
    if (wordsTechno.indexOf(word) != -1) {
      console.log(word);
      var newEl = document.createElement("p");
      var newContent = document.createTextNode(word);
      newEl.appendChild(newContent);
      list[0].appendChild(newEl);
    }
  }
}