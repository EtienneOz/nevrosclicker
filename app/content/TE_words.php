<?php

$gem = [];
$j = 1;

for ($i=97; $i<=122; $i++) {
  $gem[chr($i)] = $j;
  $j= $j+1;
}

$pathGifs = "gifs/";

$urlTechno = 'https://semestriel.framapad.org/p/nevros_noms-Techno/export/txt';


$fileTechno = file_get_contents($urlTechno);
$wordsTechno = explode (PHP_EOL, $fileTechno);

$gemTechno = [];


foreach ($wordsTechno as $key => $word) {
  $num = 0;
  $letters = mb_str_split_custom($word);
  foreach ($letters as $key =>$letter) {
    $letterClean = strtolower((stripAccents($letter)));
    $num = $num + $gem[$letterClean];
  }
  // echo '<p>' . $word . '</p>';
  $gemTechno[$num][] = $word;
}

?>