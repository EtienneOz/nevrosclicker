<main id="theo">
  
  <section class="infoBox"></section>

  <section class="mainBox">
    
    <div class="buttons">
        <input type="button" class="button buttonUp" value="">
        <input type="button" class="button buttonUpgrade" value="">
        <input type="button" class="button buttonSlot" value="">
        <input type="button" class="button buttonGlif" value="">
        <input type="button" class="button buttonFusion" value="">
      </div>

      <ul class="counter">
        <li class="counterBonus"></li>
        <li class="counterUnit">
        <?php 
          echo '<table class="pixelCounter">';
          for ($i=0; $i < 16; $i++) {
            echo '<tr>';
            for ($j=0; $j < 16; $j++) {
              echo '<td></td>';
            }
            echo '</tr>';
          }
          echo '</table>';
        ?>
          <span class="incrementUnit">0</span>
          <img src="assets/img/chrisme.bmp" alt="">
        </li>
      </ul>



      <?php      
        echo '<table class="images">';
        for ($i=0; $i < 4; $i++) {
          echo '<tr>';
          for ($j=0; $j < 4; $j++) {
            echo '<td><img src="assets/img/black.bmp" data-nb="0"></td>';
          }
          echo '</tr>';
        }
        echo '</table>';
      ?>
      
  </section>

  <section class="console"></section>
  <section class="list"><?php include('content/TE_words.php'); ?></section>

  </main>
     