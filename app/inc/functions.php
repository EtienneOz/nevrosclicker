<?php

function getTextFile($file) {
  $file = file_get_contents($file);
  $gems = explode('----', $file);
  $gems = array_filter($gems);
  foreach ($gems as $i => $gem) {
    $sentences = explode(PHP_EOL, $gem);
    $sentences = array_filter($sentences);
    $gems[$i] = $sentences;
  }
  return $gems;
}

    function unsetSessionVariable ($sessionVariableName) {
      unset($GLOBALS[_SESSION][$sessionVariableName]);
    }

    function stripAccents($str) {
      $accents = ['à','á','â','ã','ä','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ú','û','ü','ý','ÿ','À','Á','Â','Ã','Ä','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','Ù','Ú','Û','Ü','Ý'];
      $unaccents = ['a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','u','y','y','A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','U','U','U','U','Y'];
      foreach($accents as $key => $accent){
        if ($accent == $str){
          return $unaccents[$key];
        }
      }
      if(!in_array($str, $accents)){
        return $str;
      }
    }

    function mb_str_split_custom ($string, $split_length = 1)
    {
      $res = array();
      for ($i = 0; $i < mb_strlen ($string); $i += $split_length)
        $res[] = mb_substr ($string, $i, $split_length);
      return $res;
    }