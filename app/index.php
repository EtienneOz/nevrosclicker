<?php 
  // error_reporting(E_ALL);
  // ini_set('display_errors', 1);
  // ini_set('display_startup_errors', 1);

  include_once('inc/functions.php'); 
?>

<!DOCTYPE html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NevrOS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="assets/style/main.css">
  </head>
  <body>

  <?php
  $url = 'assets/lists/techmyst16.txt';
  $file = file_get_contents($url);
  $phrases= explode (PHP_EOL, $file);
  $phrases = json_encode($phrases);

  $urlTechno = 'https://semestriel.framapad.org/p/nevros_noms-Techno/export/txt';

  $fileTechno = file_get_contents($urlTechno);
  $wordsTechno = explode (PHP_EOL, $fileTechno);

  function resort($a,$b){
    return strlen($b)-strlen($a);
  }

  usort($wordsTechno,'resort');
  $wordsTechno = array_reverse($wordsTechno);
  $wordsTechno = json_encode($wordsTechno);

 ?>

 <script>
  var array = <?php echo $phrases?>;
  var wordsTechno = <?php echo $wordsTechno?>;
 </script>

    <?php include('content/TH_game.php'); ?>
    <?php include('content/TE_game.php'); ?>

    <section id="propagation"><img src="" alt=""></section>

    <section id="text"></section>

  </body>
  <script src="assets/scripts/array.js"></script>
  <script src="assets/scripts/draggable.js"></script>
  <script src="assets/scripts/makeItSpeak.js"></script>
  <script src="assets/scripts/functions.js"></script>
  <script src="assets/scripts/main.js"></script>
</html