var gem = 0;
var glif = 0;
var phrase = "";
var mot = "";
var gemArray = [];
var goodArray = ["..."];

function subGem(num) {
  gem = gem - num;
  document.getElementById("gem").innerHTML = gem;
  updateArray();
}

function addGem(num) {
  gem = num;
  console.log('gems: ' + gem)
  updateArray();
}

function subGlif(nb) {
  glif = glif - nb;
  console.log('glifs: ' + glif)
  updateArray();
}

function addGlif(nb) {
  glif = nb;
  console.log('glifs: ' + glif)
  updateArray();
}

function charToNumber(s, i) {
  return parseInt(s.charAt(i), 36) - 9;
}

function sumChars(s) {
  var i = s.length,
    r = 0;
  while (--i >= 0) r += charToNumber(s, i);
  return r;
}

function convertGem(phraseArray) {
  for (l = 0; l < phraseArray.length; l++) {
    num = sumChars(phraseArray[l]);
    gemArray[l] = num;
  }
}

function checkGem(num) {
  return num <= gem;
}

function removeDiacritics(str) {
  return str.replace(/[^\u0000-\u007E]/g, function(a) {
    return diacriticsMap[a] || a;
  });
}

function updateArray() {
  goodArray = [];
  for (j = 0; j < list.length; j++) {
    gemArray = [];
    var phrase = list[j];
    phraseClean = removeDiacritics(phrase);
    phraseClean = phraseClean.replace(/\.|\,| \?/g, "");
    phraseClean = phraseClean.replace(/\-|\'/g, " ");
    var phraseArray = phraseClean.split(" ");
    convertGem(phraseArray);
    checkGem(phraseArray);
    for (k = 0; k < phraseArray.length; k++) {
      if (gemArray.every(checkGem) == true && gemArray.length <= glif) {
        var check = true;
      } else {
        var check = false;
      }
    }
    if (check == true) {
      goodArray.push(phrase);
    }
  }
  return gem;
}

function displayTxt() {
  var rand = goodArray[Math.floor(Math.random() * goodArray.length)];
  if (rand) {
    document.getElementById("text").innerHTML = rand;
  } else {
    document.getElementById("text").innerHTML = "...";
  }

}

window.onload = function() {
  displayTxt();
};

setInterval(() => {
  displayTxt();
}, 4096);