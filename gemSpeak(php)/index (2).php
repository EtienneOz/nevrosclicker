<!DOCTYPE html>

<head>
	<script type="text/javascript" src="array.js"></script>
	<script type="text/javascript" src="main.js"></script>
</head>

<body>

	<?php
		$url = 'techmyst.txt';
		$file = file_get_contents($url);
		$phrases= explode (PHP_EOL, $file);
		$phrases = json_encode($phrases);
	?>

	<script>
		var array = <?php echo $phrases?>;
	</script>



</br></br>
	<div>Gématrie: <span id="gem">0</span></div>
	<button onclick="addGem(1)">+1</button><button onclick="subGem(1)">-1</button>
</br>
	<button onclick="addGem(10)">+10</button><button onclick="subGem(10)">-10</button>
</br>
	<button onclick="addGem(100)">+100</button><button onclick="subGem(100)">-100</button>
</br></br>

	<div>Glyphes: <span id="glif">1</span></div>
	<button onclick="addGlif(1)">+1</button><button onclick="subGlif(1)">-1</button>
	</br>
	<button onclick="addGlif(10)">+10</button><button onclick="subGlif(10)">-10</button>
</br>
<button onclick="addGlif(100)">+100</button><button onclick="subGlif(100)">-100</button>
</br></br>
	<button id= "next" onclick="displayTxt()">think()</button>
</br></br>
	<div><span id="txt"></span></div>
</br>

<link rel="stylesheet" type="text/css" href="style.css">

</body>
